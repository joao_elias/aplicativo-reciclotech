# Projeto RecicloTech
## O app funciona como um cadastro de itens eletrônicos para descarte.

<img src="https://img.shields.io/badge/RecicloTech-Projeto--do--Curso-green"/>

<p align="center">
 <a href="#objetivo">Objetivo</a> •
 <a href="#tecnologias">Tecnologias</a> •
 <a href="#licenc-a">Licença</a> • 
 <a href="#autor">Autor</a> • 
 <a href="#features">Features</a> •
 <a href="#Pre-requisitos">Pré-requisitos</a>
</p>

### Objetivo
Este projeto foi desenvolvido com o objetivo de serem postos em prática os conceitos vistos no curso FIC em Programador de Dispositivos móveis.

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Kodular](https://www.kodular.io/)

### Licença
MIT License

Copyright (c) <2021> <João Elias Ferraz Santana>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Autor
- Elias De Santana Junior
- João Elias Ferraz Santana
- Pedro Ariel
- Nataniel chagas bahia
- Isleide Bezerra Santos
- Manoel De Passos Santos Júnior

### Features

- [x] Cadastro de produtos
- [x] Remoção de produtos
- [x] Atualização de dados do produto
- [ ] Inserção de fotos do produto

### Pré-requisitos
Antes de começar, você deve dispor de um aparelho Android 4.4 ou superior com 1 GB de RAM. Faça o download do arquivo ".apk" e permita a instalação de aplicativos de outras fontes no dispositivo móvel.
